// Global Variables
/*
Date Variables
 */
var date = new Date();

var day = date.getUTCDate() - 1; // Get previous day
var month = date.getMonth() + 1; //Get current month
var year = date.getFullYear(); // Get current year
var beginingSeasonYear = new RegExp(year) - 1; // RegEx Year
var endingSeasonYear = new RegExp(year); // RegEx Year

// Start
function onStart() {

  // Call API Function
  var scoreboardData = callAPI();

  // Call Print Date Funtion
  printDate();

}

// Call API
function callAPI() {

  // Get formated date
  var date = formatDate();

  // http://data.nba.com//prod/v2/20171023/scoreboard.json
  scoreboardAPI = 'https://data.nba.net/prod/v2/' + date + '/scoreboard.json';
  //teamRostersAPI = 'http://stats.nba.com/stats/commonteamroster?LeagueID=00&Season='+ beginingSeasonYear +'&TeamID=1610612737.json';

  var config = {
    headers: {
      'X-My-Custom-Header': 'Header-Value',
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  };

  axios.all([
    axios.get(scoreboardAPI, {crossdomain: true}),
    //axios.get(teamRostersAPI),
    axios.get('data/sports-team-colors.json')
  ]).then(axios.spread(function(scoreboardAPI, teamColors,) {

    // Prepare data to be returned
    var scoreboardData = scoreboardAPI.data;

    // Prepare Team Colors data to returned
    var teamColorsData = teamColors.data;

    // Prepare Team Colors data to returned
    //  var teamRostersAPIData = teamRostersAPI.data;

    /*
      Pass data to the renderTemplate function
      to be converted into the template
      */
    renderTemplate(scoreboardData);

    // Console Log scoreboard data
    console.log(scoreboardData);
    // Console Log Team Colors data
    console.log(teamColorsData);
    // Console Log Team Colors data
    //  console.log(teamColorsData);

  })).catch(error => console.log(error));
}

// Format Date
function formatDate() {

  var getDate = year + "" + month + "" + day; // Format YYYYMMDD

  //Return Value to function
  return getDate;

}

// Print Date
function printDate() {

  // Format date to be printed to id
  var printDate = month + "/" + day + "/" + year; // Format MMDDYYYY

  // Print Date to HTML with id of 'date' using the 'printDate' variable.
  document.getElementById('date').innerHTML = printDate;
}

// Render the template
function renderTemplate(scoreboardData) {

  // Get template
  var appTemplate = document.getElementById('app-template').innerHTML;

  // pass 'raw template' to Handlebars method  to Compile Template
  var compiledTemplate = Handlebars.compile(appTemplate);

  // Pass data to template
  var getRenderedHTML = compiledTemplate(scoreboardData);

  var appContainer = document.getElementById('app-container');

  appContainer.innerHTML = getRenderedHTML;
}
